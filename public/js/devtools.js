/**
 * To check that the tools are working.
 */
'use strict'

/**
 * Return a hello message.
 * @returns {string} With hello message.
 */
function helloWorld () {
  return 'Hello World'
}

console.log(helloWorld())
