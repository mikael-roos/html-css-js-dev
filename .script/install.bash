#!/usr/bin/env bash

# Install tools
[[ -f "package.json" ]] || npm init --yes
npm install --save-dev              \
    htmlhint                        \
    stylelint                       \
    stylelint-config-recommended    \
    eslint                          \
    eslint-config-standard          \
    eslint-plugin-import            \
    eslint-plugin-jsdoc             \
    eslint-plugin-promise           \
    eslint-plugin-n                 \
    http-server                     \
    jsdoc                           \
    vite

# Download the configuration files
url="https://gitlab.com/mikael-roos/html-css-js-dev/-/raw/main"
curl --silent --output .editorconfig    $url/.editorconfig
curl --silent --output .gitignore       $url/.gitignore
curl --silent --output .stylelintrc.js  $url/.stylelintrc.js
curl --silent --output .eslintrc.js     $url/.eslintrc.js
curl --silent --output .jsdoc.json      $url/.jsdoc.json
curl --silent --output vite.config.js   $url/vite.config.js

printf "# DONE!\n"
printf "# You need to add the following script-section to your package.json.\n"
cat << EOD
"scripts": {
    "http-server": "npx http-server -p 9001",
    "dev": "vite",
    "build": "vite build",
    "serve": "vite preview",
    "htmlhint": "npx htmlhint public src || exit 0",
    "stylelint": "npx stylelint **/*.css || exit 0",
    "stylelint:fix": "npx stylelint --fix **/*.css || exit 0",
    "eslint": "npx eslint . || exit 0",
    "eslint:fix": "npx eslint --fix . || exit 0",
    "jsdoc": "npx jsdoc -c .jsdoc.json || exit 0",
    "lint": "npm run htmlhint && npm run stylelint && npm run eslint",
    "test": "npm run lint",
    "clean": "rm -rf build/",
    "clean-all": "npm run clean && rm -rf node_modules/ && rm -f package-lock.json"
}
EOD
